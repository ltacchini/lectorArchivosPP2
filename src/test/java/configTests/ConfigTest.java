package configTests;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import config.Config;

public class ConfigTest {
	
	private String configPath;
	private Config config;
	
	@Before 
	public void setUp() throws IOException{
		this.configPath = "TestConfig.properties";
		config = new DefaultConfig(configPath);
	}

	@Test
	(expected = IOException.class)
	public void testConfig() throws IOException {
		Config config = new DefaultConfig("");
		config.getElement("key");
	}
	
	@Test
	public void testGetElement() {
		String key = config.getElement("key");
		assertEquals(key, "element");
	}

	@Test
	public void testDontGetElement() {
		String key = config.getElement("IDoNotExist");
		assertEquals(key, null);
	}
	
	@Test
	public void testGetKeyColumnas() {
		String col = config.getKeyColumnas();
		assertEquals(col, "Columnas");
	}
	
}
