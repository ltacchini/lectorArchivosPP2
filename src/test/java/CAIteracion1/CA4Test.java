package CAIteracion1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import config.Config;
import config.MockConfig;
import core.DataList;
import core.DataListStreamFactory;
import filter.FieldFilter;

public class CA4Test {

	/*
	 * El primer indice contiene a A1, el segundo a A2 y así sucesivamente.
	 * sucesivamente
	 */
	List<String> linesInRow;

	@Before
	public void setUp() throws Exception {
		String filePath = "resources/hojaSimple.xlsx/Hoja 1";
		String extension = "xls";
		DataListStreamFactory lsf = new DataListStreamFactory();
		Iterable<DataList> lines = lsf.create(extension, filePath);
		FieldFilter filter = new FieldFilter();
		Config config = new MockConfig(); // Devuelve siempre las filas de Edad.
		List<String> neededFields = Arrays.asList(config.getElement(config.getKeyColumnas()).split(";"));
		List<DataList> filteredLines = filter.filter(lines, neededFields);

		this.linesInRow = new ArrayList<>();
		for (DataList d : filteredLines) {
			for (String s : d) {
				this.linesInRow.add(s);
			}
		}
	}

	@Test
	public void testa() {
		/*
		 * Se lee el primer valor y se recibe el texto “Edad", se lee el segundo valor y
		 * se recibe el texto “1”.
		 */
		String b1 = this.linesInRow.get(0);
		String b2 = this.linesInRow.get(1);

		assertEquals(b1, "Edad");
		assertEquals(b2, "1");
	}

	@Test
	public void testb() {
		/*
		 * Se lee el tercer valor y se recibe el texto “2", se lee el cuarto y último
		 * valor y se recibe el texto “3”.
		 */
		String b3 = this.linesInRow.get(2);
		String b4 = this.linesInRow.get(3);

		assertEquals(b3, "2");
		assertEquals(b4, "3");
	}

}
