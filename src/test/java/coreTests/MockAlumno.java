package coreTests;

public class MockAlumno {

	private String nombre;
	private int edad;
	private int dni;

	public int getEdad() {
		return edad;
	}

	public int getDni() {
		return dni;
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public String toString() {
		return this.nombre + this.edad;
	}
	
}
