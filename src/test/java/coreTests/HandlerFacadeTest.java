package coreTests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import enums.DataType;
import handler.Handler;
import handler.HandlerFacade;

public class HandlerFacadeTest {

	private Handler handler;
	private Object o;
	
	@Before
	public void setUp() throws Exception {
		handler = new HandlerFacade();
	}

	@Test
	public void testIntHandle() {
		o = "1";
		assertEquals(DataType.INTEGER, handler.handle(o));
	}

	@Test
	public void testStringHandle() {
		o = "String";
		assertEquals(DataType.STRING, handler.handle(o));
	}
}
