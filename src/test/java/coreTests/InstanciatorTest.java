package coreTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import core.Wrapper;

public class InstanciatorTest {

	private MockAlumno a;
	private Wrapper wp;

	@Before
	public void setUp() throws Exception {
		a = new MockAlumno();
		wp = new Wrapper("TestConfig.properties", "resources/hojaSimple.xlsx/Hoja 1", "xls");
	}

	@Test
	public void test() {
		try {
			MockAlumno al = (MockAlumno) wp.createInstances(a).get(0);
			assertEquals(al.getNombre(),"A");
		} catch (EncryptedDocumentException | NotImplementedException | InvalidFormatException | NoSuchFieldException
				| IllegalAccessException | ClassNotFoundException | InstantiationException | IOException e) {
			e.getMessage();
			fail();
		}
	}

}
