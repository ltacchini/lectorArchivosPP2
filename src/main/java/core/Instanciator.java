
package core;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import enums.DataType;

public class Instanciator {

	private Map<String, DataType> fieldToType;

	public Instanciator(Map<String, DataType> fieldToType) {
		this.fieldToType = fieldToType;
	}

	public List<Object> createInstances(String className, List<DataList> dataList)
			throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException {
		List<Object> instances = new ArrayList<>();
		List<String> values;

		dataList.remove(0);
		for (DataList list : dataList) {
			values = new ArrayList<>();
			list.forEach(values::add);

			Class<?> c = Class.forName(className);
			Object o = c.newInstance();
			for (String key : fieldToType.keySet()) {
				String value = values.remove(0);

				try {
					Field f = c.getDeclaredField(key.toLowerCase());
					f.setAccessible(true);
					if (fieldToType.get(key).equals(DataType.STRING)) {
						f.set(o, value);
					} else if (fieldToType.get(key).equals(DataType.INTEGER)) {
						f.setInt(o, Integer.parseInt(value));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			instances.add(o);
		}
		return instances;
	}
}