package core;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;

import config.Config;
import config.DefaultConfig;
import enums.DataType;
import fieldType.FieldTypeFacade;
import filter.FieldFilter;

public class Wrapper {

	private String configPath;
	private String extension;
	private String filePath;

	public Wrapper(String configPath, String filePath, String extension) {
		this.configPath = configPath;
		this.extension = extension;
		this.filePath = filePath;
	}

	public List<Object> createInstances(Object o)
			throws IOException, EncryptedDocumentException, NotImplementedException, InvalidFormatException,
			NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException {

		Config config = new DefaultConfig(configPath);
		DataListStreamFactory lsf = new DataListStreamFactory();
		Iterable<DataList> lines = lsf.create(extension, filePath);

		FieldFilter filter = new FieldFilter();
		List<String> neededFields = Arrays.asList(config.getElement(config.getKeyColumnas()).split(";"));
		
		List<DataList> filteredLines = filter.filter(lines, neededFields);

		FieldTypeFacade ftf = new FieldTypeFacade(config, filteredLines);
		Map<String, DataType> types = ftf.getTypes();
		Instanciator instanciator = new Instanciator(types);
		return instanciator.createInstances(o.getClass().getName(), filteredLines);

	}

}
