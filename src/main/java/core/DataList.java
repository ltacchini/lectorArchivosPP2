package core;

import java.util.Iterator;
import java.util.List;

public class DataList implements Iterable<String> {

	private List<String> dataList;

	public DataList(List<String> dataList) {
		this.dataList = dataList;
	}

	@Override
	public Iterator<String> iterator() {
		return this.dataList.iterator();
	}
	
}
