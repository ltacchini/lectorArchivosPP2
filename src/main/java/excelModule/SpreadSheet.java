package excelModule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import core.DataList;

public class SpreadSheet implements Iterable<DataList> {

	private Workbook workbook;
	private List<DataList> lines;

	public SpreadSheet(String filePath, String sheetName)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		this.workbook = WorkbookFactory.create(new File(filePath));
		this.lines = readSheet(sheetName);
		workbook.close();
	}

	private List<DataList> readSheet(String sheetName) throws IOException {
		List<DataList> ret = new ArrayList<>(); // Para evitar que si no hay nada en el archivo iterable tire un
		for (Sheet sheet : workbook) {
			if (sheet.getSheetName().equals(sheetName)) {
				ret = read(sheet);
			}
		}
		return ret;
	}

	private List<DataList> read(Sheet sheet) {
		DataFormatter dataFormatter = new DataFormatter();
		List<DataList> lines = new ArrayList<>();
		for (Row row : sheet) {
			List<String> line = new ArrayList<>();
			for (Cell cell : row) {
				String s = dataFormatter.formatCellValue(cell);
				if (s.equals(""))
					break;
				else
					line.add(s);
			}
			if(!line.isEmpty())
				lines.add(new DataList(line));
		}
		return lines;
	}

	public List<Sheet> getSheets() {
		List<Sheet> sheets = new ArrayList<>();
		for (Sheet sheet : workbook) {
			sheets.add(sheet);
		}
		return sheets;
	}

	public Sheet getSheet(String name) {
		return workbook.getSheet(name);
	}

	public Sheet getSheetAt(int index) {
		return workbook.getSheetAt(index);
	}

	public boolean sheetExists(String name) {
		for (Sheet sheet : workbook) {
			if (sheet.getSheetName().equals(name))
				return true;
		}
		return false;
	}

	@Override
	public Iterator<DataList> iterator() {
		return lines.iterator();
	}

}