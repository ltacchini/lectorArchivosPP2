package handler;

import enums.DataType;

public class IntHandler implements Handler {

	private Handler nextHandler;
	private DataType type = DataType.INTEGER;

	protected IntHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}
	
	@Override
	public DataType handle(Object o) {
		@SuppressWarnings("unused")
		Integer ret;
		try {
			ret = Integer.valueOf((String) o);
			return type;
		} catch (Exception e) {
			try {
				ret = Integer.parseInt((String) o);
			} catch (Exception r) {
				if (nextHandler != null)
					return nextHandler.handle(o);
			}
		}
		return null;
	}
}
